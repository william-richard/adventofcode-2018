#!/usr/bin/env ruby

require 'set'

class Claim 
    def initialize(claim_string)
        matches = claim_string.match(/\#(?<id>\d+) @ (?<x>\d+),(?<y>\d+): (?<width>\d+)x(?<height>\d+)/)
        @id = matches[:id].to_i
        @x = matches[:x].to_i
        @y = matches[:y].to_i
        @width = matches[:width].to_i
        @height = matches[:height].to_i
    end
    attr_reader :id
    attr_reader :x
    attr_reader :y
    attr_reader :width
    attr_reader :height

    def to_s
        "#{self.id} #{self.x},#{self.y} #{self.width}-#{self.height}"
    end 
end

class Fabric
    attr_reader :fabric

    def initialize claims
        # make the fabric
        # the first hash will be the x coordinate
        # the second will be the y coordinate
        # each coordinate will have a list of ids that claim that square inch

        @fabric =Hash.new{|x_hash, x_key| x_hash[x_key] = Hash.new {|y_hash, y_key| y_hash[y_key]= Array.new()}}

        puts "marking claims on the fabric"
        claims.each do |claim|
            puts claim
            (claim.x..(claim.x + claim.width - 1)).each do |current_x|
                (claim.y...(claim.y + claim.height)).each do |current_y|
                    self.fabric[current_x][current_y].push(claim.id)
                
                end
            end
        end
    end
end
    
def solve_1 fabric

    # the fabric should now be fully populated
    # count how many square inches have more than 1 claim
    puts "Finding disputed areas of the fabric"

    disputed_area_count = 0

    fabric.fabric.values.each do |inner_hash|
        inner_hash.values.each do |coordinate_claims|
            if coordinate_claims.length >= 2
                disputed_area_count += 1
            end
        end
    end
    puts disputed_area_count

end

def solve_2 fabric, claim_ids
    claims_without_overlaps = Set.new(claim_ids)

    x_count = 0
    fabric.fabric.each do |x, y_hash|
        x_count += 1
        puts x_count.to_f / fabric.fabric.keys.length.to_f * 100.0
        puts claims_without_overlaps.length
        y_hash.each do |y, coordinate_claims|
            if coordinate_claims.length >= 2
                coordinate_claims.each do |c|
                    claims_without_overlaps = claims_without_overlaps.delete(c)
                end
            end 
        end
    end

    puts claims_without_overlaps


end

claims = []
File.open("./input.txt", "r") do |f|
    f.readlines.each do |line|
        claims << Claim.new(line)
    end
end

fabric = Fabric.new(claims)

#solve_1 fabric

claim_ids = claims.collect { |c| c.id }
solve_2 fabric, claim_ids
