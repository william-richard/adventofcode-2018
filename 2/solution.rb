#!/usr/bin/env ruby

def solve_1 box_ids
    ids_with_exactly_2_letters = 0
    ids_with_exactly_3_letters = 0
    for box_id in box_ids do
        id_letter_counts = Hash.new(0)
        box_id.each_char { |c| id_letter_counts[c] += 1}
        if id_letter_counts.values.include?(2) then
            ids_with_exactly_2_letters += 1
        end
        if id_letter_counts.values.include?(3) then
            ids_with_exactly_3_letters += 1
        end
    end
    puts ids_with_exactly_2_letters, ids_with_exactly_3_letters
    puts ids_with_exactly_2_letters * ids_with_exactly_3_letters
end

class String

    def one_character_different other 
        num_different_characters = 0
        self.chars.each_index do |idx| 
            if self[idx] != other[idx] then
                num_different_characters += 1
                if num_different_characters > 1 then
                    return false
                end
            end 
        end
        if num_different_characters == 1 then
            return true            
        else
            return false
        end
    end
end

def solve_2 box_ids
    # go through all the ids
    # and look for the match
    # only need to check after this id,
    # because the match between us and all previous ids
    # were already done when the previous id was processed
    box_ids.each_with_index do |current_box_id, idx|
        box_ids_to_check = box_ids.slice(idx+1, box_ids.length)
        box_ids_to_check.each do |other_box_id|
            if current_box_id.one_character_different other_box_id then
                puts current_box_id, other_box_id
                similar_chars = []
                current_box_id.chars.each_index do |solution_idx|
                    if current_box_id[solution_idx] == other_box_id[solution_idx] then
                        similar_chars << current_box_id[solution_idx]
                    end
                end
                puts similar_chars.join("")
                exit
            end 
        end
    end

end

box_ids = Array.new()
File.open("./input.txt", "r") do |f|
    box_ids.concat f.readlines
end

solve_2 box_ids