#!/usr/bin/env ruby

require "set"

def solve_1 path
  sum = 0
  File.open(path, "r") do |f|
    f.each_line do |line|
      sum += line.to_i
    end
  end
  puts sum
end

#solve_1 "./input.txt"

def solve_2 path
  frequencies = []
  File.open(path, "r") do |f|
    f.each_line do |line| 
      frequencies.push line.to_i
    end
  end

  memory = Set.new()
  sum = 0
  while true do
    for frequency in frequencies do
      sum += frequency
      if memory.include? sum then
        puts sum
        exit
      else
        memory.add(sum)
      end
    end
  end
end


solve_2 "./input.txt"
